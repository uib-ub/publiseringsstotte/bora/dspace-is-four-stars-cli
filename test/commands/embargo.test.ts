import {expect, test} from '@oclif/test'

describe('embargo', () => {
  test.stdout().command(['embargo']).it('runs embargo', ctx => {
    expect(ctx.stdout).to.contain('CLI tool for working with the Dspace API (which is shit)')
  })

  test.stdout().command(['embargo', '--domain', 'https://bora.uib.no/bora-rest', '-l', '1', '-o', '2']).it('runs embargo --domain https://bora.uib.no/bora-rest -l 1 -o 2', ctx => {
    expect(ctx.stdout).to.contain('checking for')
  })
})
