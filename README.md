Dspace is four stars CLI
============

CLI tool for working with the Dspace API

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage
<!-- usage -->
```sh-session
$ npm install -g dspace-is-four-stars-cli
$ di4scli COMMAND
running command...
$ di4scli (-v|--version|version)
dspace-is-four-stars-cli/1.0.6 darwin-x64 node-v14.18.1
$ di4scli --help [COMMAND]
USAGE
  $ di4scli COMMAND
...
```
<!-- usagestop -->

# Commands
<!-- commands -->
* [`di4scli embargo`](#di4scli-embargo)
* [`di4scli export`](#di4scli-export)
* [`di4scli help [COMMAND]`](#di4scli-help-command)

## `di4scli embargo`

Check one item or crawl the API for embargoed items

```
USAGE
  $ di4scli embargo

OPTIONS
  -L, --level=missingDate|missingDateAndTerms  missingDate: get all items that lacks end date in a date field
                                               missingDateAndTerms: get all items without information about embargo date
                                               in terms

  -c, --collection=collection                  collectionID to get

  -d, --domain=domain                          (required) URL to check

  -i, --id=id                                  itemID to get

  -l, --limit=limit                            [default: 200] how many results

  -o, --offset=offset                          [default: 0] where should we start from

EXAMPLE
  $ di4scli embargo -d https://dspace.org --level=missingDate
```

## `di4scli export`

Export item or items from Dspace API as JSON-LD

```
USAGE
  $ di4scli export

OPTIONS
  -c, --collection=collection  collectionID to get
  -d, --domain=domain          (required) URL to check
  -f, --fromDate=fromDate      only items published after this date, e.g. 2021-01-01
  -i, --id=id                  itemID to get
  -l, --limit=limit            [default: 200] how many results
  -o, --offset=offset          [default: 0] where should we start from

EXAMPLE
  $ di4scli export -d https://dspace.org/rest
```

## `di4scli help [COMMAND]`

display help for di4scli

```
USAGE
  $ di4scli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.9/src/commands/help.ts)_
<!-- commandsstop -->
