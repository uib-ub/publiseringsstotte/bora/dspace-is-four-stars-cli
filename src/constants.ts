
// Which props should include language?
export const USE_LANG = new Set(['dc_description', 'dc_description_abstract', 'dc_title'])

// Dspace metadata schema might vary, try to make it easier to customize
export const EMBARGO_TYPE = 'fs_embargotype'
export const EMBARGO_TERMS = 'dc_embargo_terms'
export const EMBARGO_LIFTDATE = 'dc_embargo_liftdate'
export const RIGHTS_TERMS = 'dc_rights_terms'
export const RIGHTS_ACCESSRIGHTS = 'dc_rights_accessrights'
export const ISSUED = 'dc_date_issued'

