export type KeyValueLang = {
  key: string;
  value: string;
  language: string;
}

export interface JSONLDModel {
  [key: string]: any;
}

export type DspaceAPIObject = {
  id: number;
  name: string;
  handle: string;
  type: string;
  link: string;
  expand: Array<string>;
  lastModified: string;
  parentCollection: string;
  parentCollectionList: string;
  parentCommunityList: string;
  metadata: Array<KeyValueLang>;
  bitstreams: Array<any>;
  withdrawn: string;
}

export type CleanItem = {
  id: number;
  name: string;
  url: string;
  apiURL: string;
  lastModified: string;
  withdrawn: boolean;
  fsEmbargoType: string;
  fsEmbargoTerms: string;
  fsEmbargoLiftdate: string;
  dcRightsTerms: string;
  dcRightsAccessrights: string;
  metadata: JSONLDModel;
  bitstreams: Array<any> | string;
}
