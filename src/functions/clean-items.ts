import {DspaceAPIObject, CleanItem, JSONLDModel} from '../types'
import {EMBARGO_TYPE, EMBARGO_TERMS, EMBARGO_LIFTDATE, RIGHTS_TERMS, RIGHTS_ACCESSRIGHTS} from '../constants'
import {kvToJSONLD} from './kv-to-jsonld'

/**
 * Analyse chunk looking for items that could be embargoed
 * @param {array} items Array of objects
 * @param {string} domain What should we get?
 * @return {array} Returns an analysed array of items
 */

export const cleanItems = async (items: Array<DspaceAPIObject>, domain: string): Promise<CleanItem[]> => {
  const analysed: Array<CleanItem> = items.map(item => {
    const metadata: JSONLDModel = kvToJSONLD(item.metadata)
    const {host} = new URL(domain)

    const cleanItem = {
      id: item.id,
      name: item.name,
      url: `https://${host}/handle/${item.handle}`,
      apiURL: `https://${host}${item.link}?expand=metadata,bitstreams`,
      lastModified: item.lastModified,
      fsEmbargoType: metadata[EMBARGO_TYPE],
      fsEmbargoTerms: metadata[EMBARGO_TERMS],
      fsEmbargoLiftdate: metadata[EMBARGO_LIFTDATE],
      dcRightsTerms: metadata[RIGHTS_TERMS],
      dcRightsAccessrights: metadata[RIGHTS_ACCESSRIGHTS],
      withdrawn: (item.withdrawn === 'true'),
      metadata: metadata,
      bitstreams: item.bitstreams,
    } as CleanItem

    return cleanItem
  })
  return analysed.filter(Boolean)
}
