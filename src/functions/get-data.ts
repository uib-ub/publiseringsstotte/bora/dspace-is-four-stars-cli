import {DspaceAPIObject} from '../types'
import {getItems} from './get-items'
import {getItem} from './get-item'

/**
 * Wrapper for Dspace API looping
 * @param {string} domain Domain to check
 * @param {string} id ItemID
 * @param {string} collection CollectionID
 * @param {number} offset Offset
 * @param {number} limit How many results to fetch
 * @return {array} Return array of objects
 */

export const getData = async (domain: string, id: string, collection: string, offset: number, limit: number): Promise<DspaceAPIObject[]> => {
  const records: Array<DspaceAPIObject> = []

  if (!id) {
    let keepGoing = true

    while (keepGoing) {
      // eslint-disable-next-line no-await-in-loop
      const result = await getItems(domain, collection, offset, limit)

      records.push(...result)
      // Prepare next loop
      offset += limit

      if (result && result.length === 0) {
        keepGoing = false
      }
    }
  } else if (id) {
    const result = await getItem(domain, id)
    records.push(result)
  }

  return records
}
