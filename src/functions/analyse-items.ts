import {CleanItem} from '../types'
import {checkBitstreams} from './check-bitstreams'

/**
 * Analyse chunk looking for items that could be embargoed
 * @param {array} items Array of objects
 * @param {string} level What should we get?
 * @return {array} Returns an analysed array of items
 */

export const analyseItems = async (items: Array<CleanItem>, level: string): Promise<CleanItem[]> => {
  let analysed: Array<CleanItem> = []

  if (level === 'missingDate') {
    analysed = items.filter(item => (
      checkBitstreams(item.bitstreams) === false &&
      !item.withdrawn &&
      !item.fsEmbargoLiftdate &&
      !item.fsEmbargoTerms
    ))
  }

  if (level === 'missingDateAndTerms') {
    analysed = items.filter(item => (
      !checkBitstreams(item.bitstreams) &&
      !item.withdrawn &&
      !item.fsEmbargoLiftdate &&
      !item.fsEmbargoTerms &&
      !item.fsEmbargoType &&
      !item.dcRightsAccessrights &&
      !item.dcRightsTerms
    ))
  }

  return analysed
}
