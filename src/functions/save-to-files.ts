import {CleanItem} from '../types'
import fs = require('fs');
import {parse} from 'json2csv'
import chalk = require('chalk');
import logSymbols = require('log-symbols');

/**
 * Save response to filesystem
 * @param {array} data data to save
 * @param {string} filename filename to use
 * @returns {null} n
 */

export const saveToFiles = async (data: Array<CleanItem>, filename: string): Promise<void> => {
  const fields = [
    'id',
    'name',
    'url',
    'apiURL',
    'lastModified',
    'withdrawn',
    'fsEmbargoType',
    'fsEmbargoTerms',
    'fsEmbargoLiftdate',
    'dcRightsTerms',
    'dcRightsAccessrights',
    'metadata',
    'bitstreams',
  ]
  const transforms = [(item: CleanItem) => ({
    ...item, metadata: JSON.stringify(item.metadata, null, 2), bitstreams: JSON.stringify(item.bitstreams, null, 2),
  })]
  const opts = {fields, transforms}

  fs.writeFile(`${filename}.json`, JSON.stringify(data, null, 2), function (err: any) {
    if (err)
      return console.log(chalk.red(err))
    console.log('    ' + logSymbols.success + ' json written to ' + (chalk.yellow(filename + '.json')))
  })

  try {
    const csv = parse(data, opts)
    fs.writeFile(`${filename}.csv`, csv, function (err: any) {
      if (err)
        return console.log(chalk.red(err))
      console.log('    ' + logSymbols.success + ' csv written to ' + (chalk.yellow(filename + '.csv')))
    })
  } catch (error) {
    console.error(error)
  }
}
