import {ISSUED} from '../constants'
import {CleanItem} from '../types'
import {compareDates} from './compare-dates'

/**
 * Filter by date
 * @param {array} records The records to filer
 * @param {string} fromDate The date for filter by
 * @returns {array} Return the filtered records
 */

export const filterFromDate = async (records: Array<CleanItem>, fromDate: string): Promise<CleanItem[]> => {
  if (!fromDate)
    return records

  const filtered = records.filter(record => compareDates(record.metadata[ISSUED], fromDate))

  return filtered.filter(Boolean)
}
