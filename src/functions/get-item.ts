import {DspaceAPIObject} from '../types'
import axios from 'axios'
import rax = require('retry-axios')

// Dspace API is unstable and therefore we use rax to enable retries
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const interceptorID = rax.attach()

/**
 * Get single item
 * @param {string} domain Domain to check
 * @param {string} id itemID
 * @return {array} Return array of objects
 */

export async function getItem(domain: string, id: string): Promise<DspaceAPIObject | any> {
  const itemReq = {
    baseURL: domain,
    url: `/items/${id}?expand=metadata,bitstreams`,
    responsType: 'json',
    headers: {
      Accept: 'application/json',
    },
    raxConfig: {
      retry: 10,
      noResponseRetries: 4,
      retryDelay: 500,
      /* onRetryAttempt: (err: any) => {
        const cfg = rax.getConfig(err)
        console.log(chalk.blue('dis: ') + `Retry attempt #${cfg.currentRetryAttempt}`)
      }, */
    },
  }
  try {
    const response = await axios(itemReq)
    const data = await response.data
    return data
  } catch (error) {
    console.log(error)
  }
}
