/**
 * Is there a object with empty bitstream array
 * @param {array} bitstreams Array to check.
 * @return {boolean} Returns boolean.
 */

export function checkBitstreams(bitstreams: Array<any> | string): boolean {
  if (bitstreams && bitstreams.length === 0) {
    return false
  }

  return true
}
