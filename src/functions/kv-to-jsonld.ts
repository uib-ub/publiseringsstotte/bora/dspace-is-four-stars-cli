import {JSONLDModel, KeyValueLang} from '../types'
import _ = require('lodash');
import {USE_LANG} from '../constants'

/**
 * Convert dspace metadata to jsonLD
 * @param {array} data Array of objects with key, value and lang.
 * @return {object} Returns jsonLD object.
 */

export const kvToJSONLD = (data: Array<KeyValueLang>): JSONLDModel => {
  const jsonLD: JSONLDModel = {}

  for (const i of data.filter(i => i.value !== '')) {
    i.key = i.key.replace(/\./g, '_')

    if (USE_LANG.has(i.key)) {
      const lang = i.language || 'none'
      const path = [i.key, lang]
      if (_.hasIn(jsonLD, path)) {
        jsonLD[i.key][lang].push(i.value)
      } else {
        _.setWith(jsonLD, path, [i.value])
      }
    }

    if (!USE_LANG.has(i.key)) {
      if (_.has(jsonLD, i.key)) {
        if (_.isArray(jsonLD[i.key])) {
          jsonLD[i.key].push(i.value)
        }

        if (!Array.isArray(jsonLD[i.key])) {
          jsonLD[i.key] = [jsonLD[i.key]]
          jsonLD[i.key].push(i.value)
        }
      } else {
        _.setWith(jsonLD, i.key, i.value)
      }
    }
  }

  return jsonLD
}
