/**
 * Compare dates
 * @param {string} issued The date the record was issued
 * @param {string} from The date to compare to
 * @returns {boolean} Return boolean
 */

export function compareDates(issued: string, from: string): boolean {
  const iArr = issued.split('-').join(', ')
  const aArr = from.split('-').join(', ')
  const i: Date = new Date(iArr)
  const a: Date = new Date(aArr)
  const isAfter = i.getTime() >= a.getTime()
  return isAfter
}
