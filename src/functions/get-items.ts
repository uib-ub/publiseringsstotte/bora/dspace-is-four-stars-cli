import {DspaceAPIObject} from '../types'
import axios from 'axios'
import rax = require('retry-axios')

// Dspace API is unstable and therefore we use rax to enable retries
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const interceptorID = rax.attach()

/**
 * Get chunk of items from API
 * @param {string} domain Domain to check
 * @param {string} collection Domain to check
 * @param {number} offset Offset
 * @param {number} limit How many results to fetch
 * @return {array} Return array of objects
 */

export async function getItems(domain: string, collection: string, offset: number, limit: number): Promise<DspaceAPIObject | any> {
  const itemsReq = {
    baseURL: domain,
    url: `${collection ? `collections/${collection}` : ''}/items?offset=${offset}&limit=${limit}&expand=metadata,bitstreams`,
    responsType: 'json',
    headers: {
      Accept: 'application/json',
    },
    raxConfig: {
      retry: 10,
      noResponseRetries: 4,
      retryDelay: 500,
      /* onRetryAttempt: (err: any) => {
        const cfg = rax.getConfig(err)
        console.log(chalk.blue('dis: ') + `Retry attempt #${cfg.currentRetryAttempt}`)
      }, */
    },
  }
  try {
    const response = await axios(itemsReq)
    const data = await response.data
    return data
  } catch (error) {
    console.log(error)
  }
}
