/**
 * Get a timestamp
 * @return {string} Returns a timestamp
 */

export function getTimestamp(): string {
  // Create a date object with the current time
  const now = new Date()

  // Create an array with the current month, day and time
  const date = [now.getFullYear(), now.getMonth() + 1, now.getDate()].map(d => d.toString().length === 1 ? '0' + d : d)

  // Create an array with the current hour, minute and second
  const time = [now.getHours(), now.getMinutes(), now.getSeconds()].map(d => d.toString().length === 1 ? '0' + d : d)

  // Return the formatted string
  return date.join('-') + '_' + time.join('-')
}
