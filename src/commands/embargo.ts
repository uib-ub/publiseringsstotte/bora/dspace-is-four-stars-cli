import {Command, flags} from '@oclif/command'
import cli from 'cli-ux'
import {saveToFiles} from '../functions/save-to-files'
import {getTimestamp} from '../functions/get-timestamp'
import {getData} from '../functions/get-data'
import {analyseItems} from '../functions/analyse-items'
import {cleanItems} from '../functions/clean-items'
import {CleanItem} from '../types'
import Enquirer = require('enquirer')
const enquirer = new Enquirer()

import chalk = require('chalk')
import logSymbols = require('log-symbols')

export default class Embargo extends Command {
  static description = 'Check one item or crawl the API for embargoed items'

  static examples = [
    '$ di4scli embargo -d https://dspace.org --level=missingDate',
  ]

  static flags = {
    domain: flags.string({
      char: 'd',
      description: 'URL to check',
      required: true,
    }),
    id: flags.string({
      char: 'i',
      description: 'itemID to get',
      default: '',
    }),
    collection: flags.string({
      char: 'c',
      description: 'collectionID to get',
      default: '',
    }),
    level: flags.string({
      char: 'L',
      options: ['missingDate', 'missingDateAndTerms'],
      description: `missingDate: get all items that lacks end date in a date field
missingDateAndTerms: get all items without information about embargo date in terms
      `,
    }),
    limit: flags.integer({
      char: 'l',
      description: 'how many results',
      default: 200,
    }),
    offset: flags.integer({
      char: 'o',
      description: 'where should we start from',
      default: 0,
    }),
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async run() {
    const {flags} = this.parse(Embargo)
    let level = ''
    const hasLevelFlag = flags.level || ''
    if (!hasLevelFlag) {
      const responses: any = await enquirer.prompt([{
        name: 'level',
        message: 'Checking for',
        type: 'select',
        choices: [{name: 'missingDate'}, {name: 'missingDateAndTerms'}],
      }])
      level = responses.level
    } else if (hasLevelFlag) {
      level = hasLevelFlag
      this.log(chalk.bold('🔍 Checking for ') + chalk.blueBright('missingDate'))
    }

    const FILENAME = `embargo-${level}` + getTimestamp()

    cli.action.start('🕷️  Crawling the api')
    const data = await getData(flags.domain, flags.id, flags.collection, flags.offset, flags.limit)
    cli.action.stop()
    this.log('    ' + logSymbols.success + ` fetched ${data.length} documents`)

    let cleanData: Array<CleanItem> = []

    if (data.length > 0) {
      cli.action.start('🛁 Cleaning data')
      cleanData = await cleanItems(data, flags.domain)
      cli.action.stop()
    } else {
      this.log('  🤷🏽‍♀️ no results :-/')
      return
    }

    let embargoedItems: Array<CleanItem> = []

    if (cleanData.length > 0) {
      cli.action.start('🔍 Looking for embargoed documents')
      embargoedItems = await analyseItems(cleanData, level)
      cli.action.stop()
      this.log('    ' + logSymbols.success + ` found ${embargoedItems.length} documents`)
    }

    if (embargoedItems.length > 0) {
      cli.action.start('💾 Saving results')
      await saveToFiles(embargoedItems, FILENAME)
      cli.action.stop()
    }
  }
}
