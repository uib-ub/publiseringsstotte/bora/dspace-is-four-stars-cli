import {Command, flags} from '@oclif/command'
import cli from 'cli-ux'
import {saveToFiles} from '../functions/save-to-files'
import {getTimestamp} from '../functions/get-timestamp'
import {filterFromDate} from '../functions/filter-from-date'
import {getData} from '../functions/get-data'
import {cleanItems} from '../functions/clean-items'
import {CleanItem} from '../types'
import logSymbols = require('log-symbols')

export default class Export extends Command {
  static description = 'Export item or items from Dspace API as JSON-LD'

  static examples = [
    '$ di4scli export -d https://dspace.org/rest',
  ]

  static flags = {
    domain: flags.string({
      char: 'd',
      description: 'URL to check',
      required: true,
    }),
    id: flags.string({
      char: 'i',
      description: 'itemID to get',
      default: '',
    }),
    collection: flags.string({
      char: 'c',
      description: 'collectionID to get',
      default: '',
    }),
    fromDate: flags.string({
      char: 'f',
      description: 'only items published after this date, e.g. 2021-01-01',
      default: '',
    }),
    limit: flags.integer({
      char: 'l',
      description: 'how many results',
      default: 200,
    }),
    offset: flags.integer({
      char: 'o',
      description: 'where should we start from',
      default: 0,
    }),
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async run() {
    const {flags} = this.parse(Export)
    const FILENAME = 'export-' + getTimestamp()

    cli.action.start('🕷️  Crawling the api')
    const data = await getData(flags.domain, flags.id, flags.collection, flags.offset, flags.limit)
    cli.action.stop()
    this.log('    ' + logSymbols.success + ` fetched ${data.length} documents`)

    let cleanData: Array<CleanItem> = []

    if (data.length > 0) {
      cli.action.start('🛁 Creating JSON-LD')
      cleanData = await cleanItems(data, flags.domain)
      cli.action.stop()
    } else {
      this.log('🤷🏽‍♀️ No results :-/')
      return
    }

    if (flags.fromDate) {
      cli.action.start('🛁 Filtering items')
      cleanData = await filterFromDate(cleanData, flags.fromDate)
      cli.action.stop()
      this.log('    ' + logSymbols.success + ` ${cleanData.length} documents published after ${flags.fromDate}`)
    }

    if (cleanData.length > 0) {
      cli.action.start('💾 Saving results')
      saveToFiles(cleanData, FILENAME)
      cli.action.stop()
    }
  }
}
